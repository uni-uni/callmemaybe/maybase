create table public.messages
(
    messages_id    integer generated always as identity
        constraint messages_pk
            primary key,
    sending_date   timestamp       not null,
    content        text,
    sender         integer    not null
        constraint messages_users_sender_fk
            references public.users,
    "group"        integer    not null
        constraint messages_users_group_fk
            references public.groups,
    message_status varchar(15) not null
);

alter table public.messages
    owner to postgres;

