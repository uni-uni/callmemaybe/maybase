create table public.invitations
(
    invitation_id     integer generated always as identity
        constraint invitations_pk
            primary key,
    sending_date      timestamp,
    sender            integer
        constraint invitations_users_sender_fk
            references public.users,
    receiver          integer
        constraint invitations_users_null_fk
            references public.users,
    invitation_status varchar(15)
);

alter table public.invitations
    owner to postgres;

