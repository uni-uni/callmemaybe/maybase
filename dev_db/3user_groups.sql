create table public.user_groups
(
    user_group_id integer generated always as identity
        constraint user_groups_pk
            primary key,
    "user"        integer
        constraint user_groups_users_fk
            references public.users,
    "group"       integer
        constraint user_groups_groups_fk
            references public.groups
);

alter table public.user_groups
    owner to postgres;

