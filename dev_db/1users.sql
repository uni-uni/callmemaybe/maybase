create table public.users
(
    users_id      integer generated always as identity
        constraint users_pk
            primary key,
    username      varchar(75) not null
        constraint unique_username
            unique,
    email         varchar(255) not null
        constraint unique_email
            unique,
    creation_date timestamp        not null,
    user_type     varchar(15),
    user_status   varchar(15),
    icon_url      varchar(90)
);

alter table public.users
    owner to postgres;

create index users_username_index
    on public.users (username);

