create table public.connections
(
    connections_id    integer generated always as identity
        constraint connections_pk
            primary key,
    sender            integer
        constraint connections_users_sender_fk
            references public.users,
    receiver          integer
        constraint connections_users_receiver_fk
            references public.users,
    connection_status varchar(15),
    established_date  timestamp,
    invitation        integer
        constraint connections_invitations_id_fk
            references public.invitations
);

alter table public.connections
    owner to postgres;

