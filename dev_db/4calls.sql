create table public.calls
(
    call_id      integer generated always as identity
        constraint calls_pk
            primary key,
    caller       integer
        constraint calls_users_caller_fk
            references public.users,
    "group"      integer
        constraint calls_users_group_fk
            references public.groups,
    started_date timestamp,
    ended_date   timestamp,
    call_status  varchar(3)
);

alter table public.calls
    owner to postgres;

