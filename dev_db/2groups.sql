create table public.groups
(
    group_id   integer generated always as identity
        constraint groups_pk
            primary key,
    group_name varchar(75) not null
);

alter table public.groups
    owner to postgres;

